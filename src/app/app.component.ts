import { Component, OnInit } from '@angular/core';
import { Product } from './shared/model';
import { ProductService } from './shared/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  products: any;

  static productAdded: Product[] = [];

  constructor(private productService: ProductService) {
  }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts().subscribe(res => {
      if (res.status == 200) {
        this.products = res.body;
      }
      else {
        alert('Get data failed');
      }
    });
  }
}
