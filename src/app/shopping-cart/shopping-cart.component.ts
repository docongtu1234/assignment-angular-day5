import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppComponent } from '../app.component';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  listProductAdded = AppComponent.productAdded;
  sumAllProducts: number = 0;
  checkoutAddForm: FormGroup;
  success: boolean = false;

  constructor(private productService: ProductService, private fb: FormBuilder) {
    this.checkoutAddForm = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(3)]],
      phoneNumber: ['', [Validators.required, Validators.minLength(10)]],
      address: ['', [Validators.required, Validators.minLength(3)]],
      products: this.fb.array(AppComponent.productAdded)
    });
  }

  ngOnInit(): void {
    this.updateSumAllProduct();
  }

  updateQuantity(id: any, quantity: any, productSum: any) {
    AppComponent.productAdded.forEach(element => {
      if (element.id == id) {
        element.quantity = quantity;
      }
    });
    this.updateSumAllProduct();
  }

  updateSumAllProduct() {
    let newSum: number = 0;
    for (var i = 0; i < AppComponent.productAdded.length; i++) {
      newSum += AppComponent.productAdded[i].price * AppComponent.productAdded[i].quantity * (100 - AppComponent.productAdded[i].promotionPrice) / 100;
    }
    this.sumAllProducts = newSum;
  }

  deleteProduct(item: any) {
    let index: number = AppComponent.productAdded.indexOf(item);
    AppComponent.productAdded.splice(index, 1);
    this.updateSumAllProduct();
  }

  submit() {
    this.productService.addCheckout(this.checkoutAddForm.value).subscribe(res => {
      if (res.status == 200) {
        this.success = true;
        AppComponent.productAdded = [];
      }
      else {
        alert('Add failed')
      }
    });
  }

  get userName() {
    return this.checkoutAddForm.get('userName');
  }

  get phoneNumber() {
    return this.checkoutAddForm.get('phoneNumber');
  }

  get address() {
    return this.checkoutAddForm.get('address');
  }

  get products(){
    return this.checkoutAddForm.get('products');
  }
}
