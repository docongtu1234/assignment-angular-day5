import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

  getProducts(): Observable<HttpResponse<Product[]>> {
    const apiUrl = this.apiUrl + 'api/FresherFPT';
    const options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<Product[]>(apiUrl, options);
  }

  addCheckout(formData: any) {
    const apiUrl = this.apiUrl + '/api/FresherFPT/CheckOut';
    const data = JSON.stringify(formData);
    const options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.post(apiUrl, data, options);
  }
}
