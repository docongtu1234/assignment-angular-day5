import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Product } from '../shared/model';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  products: any;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts().subscribe(res => {
      if (res.status == 200) {
        this.products = res.body;
      }
      else {
        alert('Get data failed');
      }
    });
  }
  
  addToCart(product: Product) {
    var obj = {
      id: product.id,
      productName: product.productName,
      price: product.price,
      promotionPrice: product.promotionPrice,
      quantity: 1,
      image: product.image
    };

    if (AppComponent.productAdded.length == 0) {
      AppComponent.productAdded.push(obj as Product);
    }
    else {
      let index: number = -1;
      for (var i = 0; i < AppComponent.productAdded.length; i++) {
        if (AppComponent.productAdded[i].id == obj.id) {
          index = i;
          break;
        }
      }
      if (index == -1) {
        AppComponent.productAdded.push(obj);
      } else {
        AppComponent.productAdded[index].quantity += 1;
      }
    }
  }
}
